import React from 'react';
import { View, ScrollView, Text, TouchableOpacity, Image, StyleSheet, Alert } from 'react-native';
import { AntDesign } from '@expo/vector-icons';

interface CharacterDropdownProps {
    characters: any[];
    onCharacterSelect: (character: any) => void;
    onCharacterUnselect: (character: any) => void;
    searchTerm: string;
    selectedCharacters: any[];
}

const CharacterDropdown: React.FC<CharacterDropdownProps> = ({ characters, onCharacterSelect, searchTerm, selectedCharacters, onCharacterUnselect }) => {
    return (
        <ScrollView style={styles.dropdownContainer}>
            {characters.map((character, index) => (
                <TouchableOpacity key={index} style={styles.dropdownItem} onPress={() => {
                    if (selectedCharacters.some(selectedCharacter => selectedCharacter.id === character.id)) {
                        Alert.alert(
                            "Karakter Listeden Kaldırılacak",
                            "Karakteri seçili listeden kaldırmak üzeresiniz. Kaldırmak istediğinize emin misiniz?",
                            [
                              {
                                text: "Hayır",
                                style: "cancel",
                              },
                              {
                                text: "Evet",
                                onPress: () => onCharacterUnselect(character),
                              },
                            ],
                            { cancelable: false }
                          );
                    } else {
                        onCharacterSelect(character);
                    }
                }}>
                    <View style={styles.characterInfo}>
                        <View
                            style={[styles.checkBox,
                                 { backgroundColor: selectedCharacters.some(selectedCharacter => selectedCharacter.id === character.id) ? '#0075FF' : 'transparent',
                                 borderColor: selectedCharacters.some(selectedCharacter => selectedCharacter.id === character.id) ? 'transparent' : '#767676',
                                }
                            ]}
                        >
                            {selectedCharacters.some(selectedCharacter => selectedCharacter.id === character.id) && (
                                <AntDesign name="check" size={16} color="white" />
                            )}
                        </View>
                        <Image source={{ uri: character.image }} style={styles.characterImage} />
                        <View>
                            <Text>
                                {character.name.split(new RegExp(`(${searchTerm})`, 'gi')).map((part: string, i: number) => (
                                    <Text key={i} style={part.toLowerCase() === searchTerm.toLowerCase() ? { fontWeight: 'bold' } : {}}>
                                        {part}
                                    </Text>
                                ))}
                            </Text>
                            <Text style={styles.characterDetail}>{character.episode.length} Episodes</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            ))}
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    dropdownContainer: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#888',
        borderRadius: 8,
    },
    dropdownItem: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    },
    characterInfo: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        margin: 2,
    },
    characterImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
        margin: 10,
    },
    characterDetail: {
        fontSize: 12,
        color: '#888',
    },
    checkBox: {
        height: 22,
        width: 22,
        borderWidth: 2,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    }
});

export default CharacterDropdown;