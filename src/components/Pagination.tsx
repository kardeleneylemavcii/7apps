import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

interface PaginationProps {
  pagination: number;
  onNextPage: () => void;
  onPrevPage: () => void;
}

const Pagination: React.FC<PaginationProps> = ({ pagination, onNextPage, onPrevPage }) => {
  return (
    <View style={styles.pagination}>
      <TouchableOpacity onPress={onPrevPage}>
        <Text>Previous</Text>
      </TouchableOpacity>
      <Text> Page {pagination}</Text>
      <TouchableOpacity onPress={onNextPage}>
        <Text>Next</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  pagination: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    margin: 5,
  },
});

export default Pagination;