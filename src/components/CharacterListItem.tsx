import React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';

interface CharacterListItemProps {
  character: any;
  onCharacterSelect: () => void;
}

const CharacterListItem: React.FC<CharacterListItemProps> = ({ character, onCharacterSelect }) => {
  return (
    <TouchableOpacity style={styles.characterItem} onPress={onCharacterSelect}>
      <Image source={{ uri: character.image }} style={styles.characterImage} />
      <Text style={styles.characterName}>{character.name}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  characterItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  characterImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  characterName: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default CharacterListItem;