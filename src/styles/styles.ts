import { StyleSheet } from 'react-native';

export const globalStyles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    textAlign: "center",
    margin: 10
  },
  selectedCharacterItem: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    padding: 1,
    backgroundColor: '#E2E8F0',
    borderRadius: 8,
    margin: 5,
  },
  characterDetail: {
    fontSize: 12,
    color: '#888',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pagination: {
    flexDirection: "row",
    justifyContent: "space-around",
    margin: 5
  },
  
});
