import React, { useState, useRef } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Alert, ActivityIndicator, TextInput, ScrollView } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import CharacterDropdown from './src/components/CharacterDropdown';
import Pagination from './src/components/Pagination';

const App: React.FC = () => {
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [characters, setCharacters] = useState<any[]>([]);
  const [selectedCharacters, setSelectedCharacters] = useState<any[]>([]);
  const [pagination, setPagination] = useState<number>(1);
  const [showDropdown, setShowDropdown] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const searchInputRef = useRef<TextInput>(null);
  

  const fetchCharacters = async (term: string, page: number) => {
    setLoading(true);
    try {
      let response;
      if (term.trim() !== '') {
        response = await fetch(`https://rickandmortyapi.com/api/character/?name=${term}&page=${page}`);
      } else {
        response = await fetch(`https://rickandmortyapi.com/api/character/?page=${page}`);
      }
      const data = await response.json();
      if (data.results) {
        setCharacters(data.results);
        setShowDropdown(true);
      } else {
        setCharacters([]);
        setShowDropdown(false);
        Alert.alert('Bulunamadı', 'Karakter bulunamadı');
      }
    } catch (error) {
      Alert.alert('Hata', 'Karakter verileri çekilirken bir hata meydana geldi, internet bağlantınızı kontrol edin.');
      console.error('Error fetching characters:', error);
    } finally {
      setLoading(false);
    }
  };

  const handleSearch = (term: string) => {
    setSearchTerm(term);
    setPagination(1);
    fetchCharacters(term, 1);
  };

  const handlePaginationIncrease = () => {
    if (characters.length === 20) {
      setPagination(prevPagination => prevPagination + 1);
      fetchCharacters(searchTerm, pagination + 1);
    } else {
      Alert.alert('Bulunamadı', 'Veri Bulunamadı')
    }
  };

  const handlePaginationDecrease = () => {
    if (pagination > 1) {
      setPagination(prevPagination => prevPagination - 1);
      fetchCharacters(searchTerm, pagination - 1);
    }
  };

  const handleAddCharacter = (character: any) => {
    const isCharacterSelected = selectedCharacters.some(
      (selectedCharacter) => selectedCharacter.id === character.id
    );

    if (!isCharacterSelected) {
      setSelectedCharacters([...selectedCharacters, character]);
    } else {
      Alert.alert('Karakter Seçilemedi', 'Karakter önceden seçildiği için seçilemedi')
    }
  };

  const handleRemoveCharacter = (characterToRemove: any) => {
    const indexToRemove = selectedCharacters.findIndex(selectedCharacter => selectedCharacter.id === characterToRemove.id);
    if (indexToRemove !== -1) {
      const updatedCharacters = [...selectedCharacters];
      updatedCharacters.splice(indexToRemove, 1);
      setSelectedCharacters(updatedCharacters);
    }
  };
  return (
    <View style={styles.container}>
      <Text style={styles.title}>RICK AND MORTY</Text>

      <View style={styles.searchContainer}>
        <ScrollView style={{maxHeight:100}}>
          <View style={styles.selectedCharacterList}>
            {selectedCharacters.map((character, index) => (
              <TouchableOpacity key={index} style={styles.selectedCharacterItem}
                onPress={() => {
                  Alert.alert(
                    "Karakter Listeden Kaldırılacak",
                    "Karakteri seçili listeden kaldırmak üzeresiniz. Kaldırmak istediğinize emin misiniz?",
                    [
                      {
                        text: "Hayır",
                        style: "cancel",
                      },
                      {
                        text: "Evet",
                        onPress: () => handleRemoveCharacter(character),
                      },
                    ],
                    { cancelable: false }
                  );
                }}>
                <Text style={styles.characterName}>{character.name}</Text>
                <AntDesign name="closesquare" size={24} color="#94A3B8" />

              </TouchableOpacity>
            ))}

            <TextInput
              style={styles.searchInput}
              placeholder="Search..."
              placeholderTextColor="#888"
              value={searchTerm}
              onChangeText={(text) => handleSearch(text)}
            />
          </View>
        </ScrollView>
      </View>

      {loading ? (
        <View style={styles.loadingContainer}>
          <ActivityIndicator size="large" color="#000000" />
        </View>
      ) : showDropdown ? (
        <CharacterDropdown
          characters={characters}
          selectedCharacters={selectedCharacters}
          onCharacterSelect={handleAddCharacter}
          onCharacterUnselect={handleRemoveCharacter}
          searchTerm={searchTerm}
        />
      ) : null}

      <Pagination
        pagination={pagination}
        onNextPage={handlePaginationIncrease}
        onPrevPage={handlePaginationDecrease}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 15,
    margin: 5
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    textAlign: "center",
    margin: 10
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchContainer: {
    marginVertical: 10,
    borderWidth: 1,
    borderColor: '#888',
    borderRadius: 8
  },
  searchInput: {
    minWidth: '25%',
    fontWeight: "bold",
    margin: 5
  },
  selectedCharacterList: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  selectedCharacterItem: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    padding: 5,
    backgroundColor: '#E2E8F0',
    borderRadius: 8,
    margin: 5,
  },
  characterName: {
    fontSize: 16,
    fontWeight: 'bold',
    margin: 1
  },
});

export default App;